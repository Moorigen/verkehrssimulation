﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class KeyControls : MonoBehaviour {

    [SerializeField] InputField numberIn;

    void Update() {
        if (Input.GetKeyDown(KeyCode.Escape)) {
            ClickedObject.Deselect();
        }

        if (Input.GetKeyDown(KeyCode.UpArrow)) {
            ClickedObject.Increase();
        }

        if (Input.GetKeyDown(KeyCode.Backspace)) {
            ClickedObject.Deletion();
        }

        if (Input.GetKeyDown(KeyCode.KeypadEnter)) {
            FindObjectOfType<Navigator>().StartNavigation(GameObject.Find("TestStart").transform.position, GameObject.Find("TestEnd").transform.position);
        }


        if (Input.GetKeyDown(KeyCode.PageUp)) {
            if (!numberIn.text.Equals("")) {
                FileReader.instance.LoadTransportationData("Planfahrt_cleaned.csv", long.Parse(numberIn.text));
            } else {
                FileReader.instance.LoadTransportationData("Planfahrt_cleaned.csv");
            }
        }

        if (Input.GetKeyDown(KeyCode.PageDown)) {
            FileReader.instance.LoadPositionData("stations.tsv");
        }

        if (Input.GetKeyDown(KeyCode.Delete)) {
            DataBaseController.instance.ClearDB();
        }
    }
}
