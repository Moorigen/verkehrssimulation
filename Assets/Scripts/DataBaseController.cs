﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Data;
using Mono.Data.Sqlite;
using System.IO;

public class DataBaseController : MonoBehaviour {

    public static DataBaseController instance;
    private void Awake() {
        instance = this;
    }

    IDbConnection dbConn;
    IDataReader reader;
    IDbCommand dbCMD;

    private const string testDbName = "Fahrgastzaehlung_CVAG";

    //Opens connection to DB
    void OpenDBConnection(string dbName) {
        string conn = "URI=file:" + System.IO.Path.Combine(Application.streamingAssetsPath, "DB/" + dbName + ".sqlite");
        dbConn = (IDbConnection)new SqliteConnection(conn);
        dbConn.Open();
    }

    //Request all Information from one table
    public void Request(string dbName, string table) {
        OpenDBConnection(dbName);
        dbCMD = dbConn.CreateCommand();
        IDataReader reader;
        string query = "SELECT * FROM " + table;
        dbCMD.CommandText = query;
        reader = dbCMD.ExecuteReader();
        while (reader.Read()) {
            Debug.Log("1: " + reader[0].ToString());
            Debug.Log("2: " + reader[1].ToString());
            Debug.Log("3: " + reader[2].ToString());
            Debug.Log("4: " + reader[3].ToString());
        }
        CloseDBConnection();
    }

    //Methode zum Schreiben in die Datenbank ||| Als Blueprint reinkopiert
    public void WriteToDB(string query) {
        OpenDBConnection(testDbName);
        dbCMD = dbConn.CreateCommand();
        dbCMD.CommandText = query;
        dbCMD.ExecuteNonQuery();
        CloseDBConnection();
    }

    public void AddStationData(int id, string name, Vector2 position) {
        id = MakeIdUnique(id, "Fahrgastzaehlung_CVAG", "stations", "StationID");
        OpenDBConnection("Fahrgastzaehlung_CVAG");
        dbCMD = dbConn.CreateCommand();
        dbCMD.CommandText = $"insert into STATIONS (StationID, name, latitude, longitude) values ({id}, '{name}', '{position.x}', '{position.y}')";
        dbCMD.ExecuteNonQuery();
        CloseDBConnection();
    }

    private int MakeIdUnique(int id, string db, string table, string idDataName) {
        OpenDBConnection(db);

        dbCMD = dbConn.CreateCommand();
        dbCMD.CommandText = $"select {idDataName} from {table} where {idDataName} = {id}";
        var t = dbCMD.ExecuteScalar();

        if (t != null) {
            dbCMD = dbConn.CreateCommand();
            dbCMD.CommandText = $"select MAX({idDataName}) from {table}";
            id = int.Parse(dbCMD.ExecuteScalar().ToString());
            id++;
        } 

        CloseDBConnection();
        return id;
    }

    public void AddLineIfNotExisting(int id) {
        OpenDBConnection("Fahrgastzaehlung_CVAG");

        dbCMD = dbConn.CreateCommand();
        dbCMD.CommandText = $"select number from LINE where number = {id}";
        var t = dbCMD.ExecuteScalar();

        if (t == null) {
            dbCMD = dbConn.CreateCommand();
            dbCMD.CommandText = $"insert into LINE (number) values ({id})";
            dbCMD.ExecuteNonQuery();
        }

        CloseDBConnection();
    }

    public int FindIdOfEntryInTable(string entry, string db, string table, string entryDataName) {
        OpenDBConnection(db);

        dbCMD = dbConn.CreateCommand();
        dbCMD.CommandText = $"select ID from {table} where {entryDataName} = '{entry}'";
        //Debug.Log(dbCMD.CommandText);
        
        var t = dbCMD.ExecuteScalar();


        //Debug.Log($"Found for {entry} in {table} as {entryDataName}: {t.ToString()}");

        int r;
        if (t != null) {
            r = int.Parse(t.ToString());
        } else {
            r = -1;
        }
        
        CloseDBConnection();

        return r;
    }

    public void AddStationOfLineIfNotExisting(int lineKey, int stationKey, int order) {
        OpenDBConnection("Fahrgastzaehlung_CVAG");

        dbCMD = dbConn.CreateCommand();
        //dbCMD.CommandText = $"select ID from LINE_STATIONS where StationID = '{station}' and LineID = '{line}' and ORDER = '{order}'";
        dbCMD.CommandText = $"select LineID from LINE_STATIONS where StationID = '{stationKey}' and LineID = '{lineKey}'";
        //Debug.Log(dbCMD.CommandText);

        var t = dbCMD.ExecuteScalar();

        if (t == null) {
            // Debug.Log($"Add new {lineKey} with {stationKey} at {order}");
            dbCMD = dbConn.CreateCommand();
            dbCMD.CommandText = $"insert into LINE_STATIONS (LineID, StationID, sequenced) values ({lineKey}, {stationKey}, '{order}')";
            //dbCMD.CommandText = $"insert into LINE_STATIONS (LineID, StationID) values ({lineKey}, {stationKey})";
            dbCMD.ExecuteNonQuery();
        }

        CloseDBConnection();
    }

    public void AddDataCollection(string date, int lineKey, int stationKey, int direction, string targetTime, string isTime, int passenger_in, int passenger_out, int passenger_on) {
        OpenDBConnection("Fahrgastzaehlung_CVAG");

        dbCMD = dbConn.CreateCommand();
        dbCMD.CommandText = $"insert into DATA_COLLECTION (recordDate, LineNumber, direction, target_time, is_time, StationID, passenger_in, passenger_out, passenger_on) values " +
            $"('{date}', {lineKey}, {direction}, '{targetTime}', '{isTime}', {stationKey}, {passenger_in}, {passenger_out}, {passenger_on})";
        dbCMD.ExecuteNonQuery();

        CloseDBConnection();
    }

    public void ClearDB(bool clearStations = true) {
        OpenDBConnection(testDbName);
        if (clearStations) {
            dbCMD = dbConn.CreateCommand();
            dbCMD.CommandText = "delete from STATIONS";
            dbCMD.ExecuteNonQuery();
        }
        dbCMD.CommandText = "delete from LINE";
        dbCMD.ExecuteNonQuery();
        dbCMD = dbConn.CreateCommand();
        dbCMD.CommandText = "delete from LINE_STATIONS";
        dbCMD.ExecuteNonQuery();
        dbCMD = dbConn.CreateCommand();
        dbCMD.CommandText = "delete from DATA_COLLECTION";
        dbCMD.ExecuteNonQuery();
        CloseDBConnection();
    }

    //closes connection to DB
    void CloseDBConnection() {
        dbCMD.Dispose();
        dbCMD = null;
        dbConn.Close();
        dbConn = null;
    }
}
