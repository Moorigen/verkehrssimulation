﻿using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using UnityEngine;

public static class Parser 
{
    public static (int, string) GetStationIdAndName(string line, int idPos = 6, int namePos = 8) {
        string[] lines = line.Split(';');

        // Debug.Log($"parsing {line}");

        DataBaseController dbc = DataBaseController.instance;

        dbc.AddLineIfNotExisting(int.Parse(lines[1]));

        int lineKey = dbc.FindIdOfEntryInTable(lines[1], "Fahrgastzaehlung_CVAG", "LINE", "number");
        int stationKey = dbc.FindIdOfEntryInTable(lines[8], "Fahrgastzaehlung_CVAG", "STATIONS", "name");
        try {
            dbc.AddStationOfLineIfNotExisting(lineKey, stationKey, int.Parse(lines[5]));

            dbc.AddDataCollection(lines[0], lineKey, stationKey, int.Parse(lines[2]), lines[4], lines[9], (int)float.Parse(lines[10]), (int)float.Parse(lines[11]), (int)float.Parse(lines[12]));
        }
        catch (System.Exception) {
            Debug.LogWarning("Error parsing: " + line);
            throw;
        }
        

        int id = int.MinValue;
        if (!int.TryParse(lines[idPos], out id)) {
            Debug.LogWarning("Error parsing: " + line);
            return (id, "Error");
        }
        return (id, lines[namePos]);
    }

    public static (int, string, Vector2) GetStationPosition(string line,int idPos = 0, int namePos = 1, int posPos = 2) {
        string[] lines = line.Split('\t');
        string posData = lines[posPos];
        posData = posData.Remove(0, 6);
        posData = posData.Remove(posData.Length - 2);
        float x, y;
        string[] posFs = posData.Split(' ');
        x = posFs[0].ParseToFloat();
        y = posFs[1].ParseToFloat();
        string name = lines[namePos];
        name = name.Replace("\"", "");

        //name = name.Replace(',', '-');

        int id = int.Parse(lines[idPos]);
        return (id, name, new Vector2(x, y));
    }
}
