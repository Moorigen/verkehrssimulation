﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LineText : Clickable
{
    public Transform edge;
    public Line line;
    
    protected override void OnMouseDrag() {
        if (isDragging || Vector3.Distance(mouseDownPos, Camera.main.ScreenToWorldPoint(Input.mousePosition)) > dragDistance) {
            if (!isDragging) {
                isDragging = true;
                //ClickedObject.Deselect();
                ClickedObject.Select(gameObject);
            }
            Vector3 mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            transform.position = new Vector3(mousePos.x, mousePos.y, zPos);
        }
        if (line) {
            line.Redraw();
        }
    }

}
