﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Text;
using UnityEngine;

public class FileReader : MonoBehaviour
{
    public static FileReader instance;
    private void Awake() {
        instance = this;
    }

    List<string> lines;

    public void LoadTransportationData(string name, long number = long.MaxValue) {
        DataBaseController.instance.ClearDB(false);
        FileStream file = File.OpenRead($"Assets{Path.DirectorySeparatorChar}Data{Path.DirectorySeparatorChar}{name}");
        lines = new List<string>();
        StartCoroutine(ReadAll(file, "ParseLinesAsTransportationData", number));
    }

    public void LoadPositionData(string name) {
        FileStream file = File.OpenRead($"Assets{Path.DirectorySeparatorChar}Data{Path.DirectorySeparatorChar}{name}");
        //stations = new Dictionary<string, Vector2>();
        lines = new List<string>();
        StartCoroutine(ReadAll(file, "ParseLinesToPositionData"));
    }


    private string ReadLine(FileStream fs) {
        int breaker = 0;
        string o = "";
        UTF8Encoding uTF8 = new UTF8Encoding();
        byte[] b = new byte[1];
        string t = "";
        while (!t.Equals("\n")) {
            fs.Read(b, 0, 1);
            t = uTF8.GetString(b);
            o += t;
            breaker++;
            if (breaker>5000) {
                break;
            }
        }
        return o;
    }

    bool log = false;
    IEnumerator ReadAll(FileStream fs, string followUpRoutine, long maxCount = long.MaxValue) {
        int count = 0;
        long fileLength = maxCount < fs.Length ? maxCount : fs.Length;
        Invoke("Timer", 1f);
        while (fs.Position < fs.Length && count < maxCount) {
            for (int i = 0; i < 1000; i++) {
                lines.Add(ReadLine(fs));
                count++;
                if (fs.Position == fs.Length) {
                    break;
                }
            }

            if (log) {
                log = false;
                Invoke("Timer", 1f);
                Debug.Log($"Read {count} lines, equaling {100f * fs.Position / fs.Length}%. Time: {Time.realtimeSinceStartup}");
            }
            yield return null;
        }
        Debug.Log($"Read {count} lines, equaling {100f * fs.Position / fs.Length}%. Time: {Time.realtimeSinceStartup}");
        fs.Flush();
        fs.Close();
        StartCoroutine(followUpRoutine);
    }

    IEnumerator ParseLinesAsTransportationData() {
        lines.RemoveAt(0);
        int count = 0;
        int lCount = lines.Count - 1;
        Debug.Log($"Starting Parse Operation. Time: {Time.realtimeSinceStartup}");
        while (count < lCount) {
            if (lines.Count - count > 100) {
                for (int i = 0; i < 100; i++) {
                    count++;
                    DataManager.CreateOrGetStation(Parser.GetStationIdAndName(lines[count]));
                }
            } else {
                for (int i = count; i < lCount; i++) {
                    count++;
                    DataManager.CreateOrGetStation(Parser.GetStationIdAndName(lines[count]));
                }
            }
            
            if (log) {
                log = false;
                Invoke("Timer", 1f);
                Debug.Log($"Parsing line {count}, parsed {100f * count / lines.Count}%. Time: {Time.realtimeSinceStartup}");
            }
            yield return null;
        }
        Debug.Log($"Finished parsing at line {count}. Time: {Time.realtimeSinceStartup}");
        yield return null;
        DataManager.SortStations();
    }

    IEnumerator ParseLinesToPositionData() {
        lines.RemoveAt(0);
        int count = 0;
        int lCount = lines.Count - 1;
        Debug.Log($"Starting Parse Operation. Time: {Time.realtimeSinceStartup}");
        while (count < lCount) {
            if (lines.Count - count > 1000) {
                for (int i = 0; i < 1000; i++) {
                    count++;
                    var t = Parser.GetStationPosition(lines[count]);
                    //Debug.Log($"Added {t.Item1} at {t.Item2}");
                    DataManager.AddStationPosition(t.Item1, t.Item2, t.Item3);
                }
            } else {
                for (int i = count; i < lCount; i++) {
                    count++;
                    var t = Parser.GetStationPosition(lines[count]);
                    DataManager.AddStationPosition(t.Item1, t.Item2, t.Item3);
                }
            }

            if (log) {
                log = false;
                Invoke("Timer", 1f);
                Debug.Log($"Parsing line {count}, parsed {100f * count / lines.Count}%. Time: {Time.realtimeSinceStartup}");
            }
            yield return null;
        }
        Debug.Log($"Finished parsing at line {count}. Time: {Time.realtimeSinceStartup}");
        yield return null;
    }

    void Timer() {
        log = true;
    }

}
