﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DBTester : MonoBehaviour
{
    private DataBaseController dataBaseController;

    void Start()
    {
        dataBaseController = DataBaseController.instance;
    }
    void Update()
    {
        if(Input.GetKeyDown( KeyCode.Space )) {
            dataBaseController.Request("Fahrgastzaehlung_CVAG", "STATIONS" );
        }
    }
}
