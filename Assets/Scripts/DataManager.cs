﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public static class DataManager {

    private static Dictionary<int, GameObject> stations = new Dictionary<int, GameObject>();

    private static Dictionary<string, Vector2> stationPositions = new Dictionary<string, Vector2>();
    public static void AddStationPosition(int id, string name, Vector2 pos) {
        stationPositions.Add(name, pos);
        DataBaseController.instance.AddStationData(id, name, pos);
    }
    public static Vector2 GetStationPosition(string name) {
        if (stations != null) {
            if (stationPositions.ContainsKey(name)) {
                return stationPositions[name];
            }
        }
        return Vector2.zero;
    }

    public static GameObject CreateOrGetStation(int id, string name = null) {
        if (stations.ContainsKey(id)) {
            return stations[id];
        } else {
            GameObject gameObject = new GameObject("Station: " + name + "|" + id);
            gameObject.transform.SetParent(GameObject.Find("Stations").transform);
            gameObject.transform.position = GetStationPosition(name);
            stations.Add(id, gameObject);
            return gameObject;
        }
    }

    public static GameObject CreateOrGetStation((int, string) data) {
        if (stations.ContainsKey(data.Item1)) {
            return stations[data.Item1];
        } else {
            GameObject gameObject = new GameObject("Station: " + data.Item2 + "|" + data.Item1);
            gameObject.transform.SetParent(GameObject.Find("Stations").transform);
            gameObject.transform.position = GetStationPosition(data.Item2);
            stations.Add(data.Item1, gameObject);
            return gameObject;
        }
    }

    public static void SortStations() {

        Dictionary<int, GameObject> copy = new Dictionary<int, GameObject>();

        foreach (var item in stations) {
            copy.Add(item.Key, item.Value);
        }
        
        List<int> keys = stations.Keys.ToList();
        keys.Sort();

        stations.Clear();
        for (int i = 0; i < keys.Count; i++) {
            int key = keys[i];
            GameObject go = copy[key];
            go.transform.SetSiblingIndex(i);
            stations.Add(key, go);
        }
    }

    private static Dictionary<NodePair, Line> lines = new Dictionary<NodePair, Line>();

    public static bool AddLine(NodePair nodes, Line line) {
        if (lines.ContainsKey(nodes)) {
            return false;
        } else {
            lines.Add(nodes, line);
            return true;
        }
    }

    public static void RemoveLine(Line line) {
        foreach (var entry in lines) {
            if (entry.Value.Equals(line)) {
                lines.Remove(entry.Key);
                return;
            }
        }
    }

    public static void SetAsDirty(GameObject node) {
        foreach (var entry in lines) {
            if (entry.Key.a.Equals(node) || entry.Key.b.Equals(node)) {
                entry.Value.Redraw();
            }
        }
    }

    public static Line GetLine(NodePair pair) {
        lines.TryGetValue(pair, out Line val);
        return val;
    }

    public static List<Line> GetLines(GameObject obj) {
        List<Line> rLines = new List<Line>();

        foreach (var entry in lines) {
            if (entry.Key.Contains(obj)) {
                rLines.Add(entry.Value);
            }
        }

        return rLines;
    }
}
