﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Collider2D))]
public class Clickable : MonoBehaviour {

    protected Vector3 mouseDownPos;
    protected bool isDragging;

    protected static float dragDistance = 0.35f;

    protected float zPos;

    void Start() {
        zPos = transform.position.z;
    }

     private void OnMouseUpAsButton() {
        isDragging = false;
        if (Vector3.Distance(mouseDownPos, Camera.main.ScreenToWorldPoint(Input.mousePosition)) < dragDistance) {
            ClickedObject.Select(gameObject);
        }
    }

    private void OnMouseDown() {
        mouseDownPos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
    }

    protected virtual void OnMouseDrag() {
        if (isDragging || Vector3.Distance(mouseDownPos, Camera.main.ScreenToWorldPoint(Input.mousePosition)) > dragDistance) {
            if (!isDragging) {
                isDragging = true;
                ClickedObject.Deselect();
            }
            Vector3 mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            transform.position = new Vector3(mousePos.x, mousePos.y, zPos);
        }
    }
}
