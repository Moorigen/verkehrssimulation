﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Node : Clickable, IDestructable {
    public bool isSpawnNode = true;
    public static int num = 1;

    protected override void OnMouseDrag() {
        if (isDragging || Vector3.Distance(mouseDownPos, Camera.main.ScreenToWorldPoint(Input.mousePosition)) > dragDistance) {
            if (!isDragging) {
                isDragging = true;
                ClickedObject.Deselect();
                if (isSpawnNode) {
                    isSpawnNode = false;
                    num++;
                    Instantiate(Resources.Load<GameObject>("Node"), GameObject.Find("NodeSpawn").transform.position, Quaternion.identity, transform.parent).name = "Node-" + num;
                }
            }
            Vector3 mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            transform.position = new Vector3(mousePos.x, mousePos.y, zPos);
            DataManager.SetAsDirty(gameObject);
        }
    }

    public void Destroy() {
        List<Line> lines = DataManager.GetLines(gameObject);
        foreach (Line line in lines) {
            line.Destroy();
        }
        Destroy(gameObject);
    }
}
