﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public static class ClickedObject {
    private static GameObject selected;
    private static Color originalSelectionColor;

    private static Color highlightColor = Color.green;

    private static Color lineColor = Color.gray;

    public static void Select(GameObject selection) {

        if (selected) {
            if (selection.Equals(selected)) {
                return;
            }

            if (selected.GetComponent<Node>() && selection.GetComponent<Node>()) {
                if (!DataManager.GetLine(new NodePair(selected, selection))) {
                    Line l = DataManager.GetLine(new NodePair(selection, selected));
                    if (l) {
                        l.GetComponent<Image>().sprite = Resources.Load<Sprite>("DoubleArrow");
                        l.duoDirections = true;
                        return;
                    }
                    DrawLine.DrawLineBetween(selection.transform.parent, selected.transform, selection.transform, lineColor);
                    Deselect();
                    return;
                }
            }
        }

        Text t = selection.GetComponent<Text>();
        if (t) {
            LineText lt = selection.GetComponent<LineText>();
            lt.line = DrawLine.DrawLineBetween(selection.transform.parent, selection.transform, lt.edge, Color.red, false, 0.6f).GetComponent<Line>();

            Deselect();
            originalSelectionColor = t.color;
            t.color = highlightColor;

        } else {
            if (!(selected && selected.Equals(selection))) {
                Deselect();
            }
            Image img = selection.GetComponent<Image>();
            originalSelectionColor = img.color;
            img.color = highlightColor;
        }
        selected = selection;
    }

    public static void Deselect() {
        if (selected) {
            Text t = selected.GetComponent<Text>();
            if (t) {
                t.color = originalSelectionColor;
                Line line = t.GetComponent<LineText>().line;
                if (line) {
                    GameObject.Destroy(line.gameObject);
                }
            } else {
                selected.GetComponent<Image>().color = originalSelectionColor;
            }
            selected = null;
        }
    }

    public static void Increase() {
        if (selected) {
            Line l = selected.GetComponent<Line>();
            if (l) {
                l.flow++;
                GameObject.Find(l.name + "T").GetComponent<Text>().text = l.flow.ToString();
            }
        }
    }

    public static void Deletion() {
        try {
            selected.GetComponent<IDestructable>().Destroy();
        }
        catch (System.Exception) {
        }
    }
}
