﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Navigator : MonoBehaviour
{
    public void StartNavigation(Vector3 startPos, Vector3 endPos) {
        NavMeshAgent nma = GetComponent<NavMeshAgent>();

        transform.position = startPos;
        nma.SetDestination(endPos);
    }
}
