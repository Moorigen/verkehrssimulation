﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Extensions {
    public static string toString(this Dictionary<NodePair, LineData> dict) {
        string val = "";
        foreach (var entry in dict) {
            val += $"Line: {entry.Key.a.name}-{entry.Key.b.name} | {entry.Value.line.name} at {entry.Value.flow} \n";
        }
        return val;
    }

    public static float ParseToFloat(this string s) {
        s = s.Replace(',', '.');
        if (!s.Contains(".")) {
            return float.Parse(s);
        } else {
            string[] a = s.Split('.');
            float f = 0f;

            for (int i = 0; i < a[1].Length; i++) {
                f += int.Parse(a[1][i].ToString()) / Mathf.Pow(10f, i + 1);
            }
            
            for (int i = a[0].Length - 1; i >= 0; i--) {
                f += int.Parse(a[0][i].ToString()) * Mathf.Pow(10f, a[0].Length - (i + 1));
            }

            return f;
        }
    }
}

public struct NodePair {
    public GameObject a;
    public GameObject b;

    public NodePair(GameObject a, GameObject b) {
        this.a = a;
        this.b = b;
    }

    public bool Contains(GameObject obj) {
        if (obj.Equals(a) || obj.Equals(b)) {
            return true;
        }
        return false;
    }
}

public struct LineData {
    public GameObject line;
    public float flow;

    public LineData(GameObject line, float flow = 0f) {
        this.line = line;
        this.flow = flow;
    }
}
