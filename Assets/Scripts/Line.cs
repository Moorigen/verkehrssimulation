﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Line : MonoBehaviour, IDestructable {
    private Transform start, end;
    public NodePair GetNodePair() {
        return new NodePair(start.gameObject, end.gameObject);
    }

    public float flow;

    public bool duoDirections = false;

    private bool isEdge;

    private float width;

    public void Init(Transform start, Transform end, bool edge, float width) {
        this.start = start;
        this.end = end;
        this.width = width;
        transform.localScale = Vector3.one;
        isEdge = edge;
        GetComponent<UnityEngine.UI.Image>().sprite = Resources.Load<Sprite>("SingleArrow") as Sprite;
        gameObject.AddComponent<BoxCollider2D>().size = GetComponent<RectTransform>().sizeDelta;
        Redraw();
    }
    
    public void Redraw() {
        float dist = Vector3.Distance(start.position, end.position);
        //Debug.Log(dist);
        float sY = start.position.y;
        float eY = end.position.y;
        float yDiff = eY > sY ? eY - sY : sY - eY;
        //float trans = (Mathf.PI / 2f) * (yDiff / dist);
        //Debug.Log($"{yDiff}/{dist}={yDiff/dist}");
        //float rot = Mathf.Sin(trans) * 90f;
        //Debug.Log($"Sin({yDiff * Mathf.Rad2Deg / dist})={Mathf.Sin(yDiff * Mathf.Rad2Deg / dist)}");
        float rel = Vector2.Angle(Vector2.up, start.transform.position - end.transform.position) + 90f;
        //float rot = Mathf.Sin(rel * 0.5f * Mathf.PI) * 90f;
        //Debug.Log(rel);
        if (start.position.x < end.position.x)
            transform.SetPositionAndRotation((start.position + end.position) / 2f, Quaternion.Euler(0f, 0f, rel));
        else
            transform.SetPositionAndRotation((start.position + end.position) / 2f, Quaternion.Euler(0f, 0f, -rel));

        //transform.localScale = new Vector3(dist, width, width);
        RectTransform rt = GetComponent<RectTransform>();
        rt.sizeDelta = new Vector2(100f * dist, 100f * width);
        gameObject.GetComponent<BoxCollider2D>().size = rt.sizeDelta;
        if (start.position.x > end.position.x && transform.localScale.x > 0f) {
            transform.localScale = new Vector3(-transform.localScale.x, transform.localScale.y, transform.localScale.z);
        } else if (start.position.x < end.position.x && transform.localScale.x < 0f) {
            transform.localScale = new Vector3(-transform.localScale.x, transform.localScale.y, transform.localScale.z);
        }
    }

    private void OnMouseUpAsButton() {
        ClickedObject.Select(gameObject);
        Debug.Log(this.flow);
    }

    public void Destroy() {
        if (isEdge) {
            Destroy(GameObject.Find(transform.name + "T"));
            DataManager.RemoveLine(this);
            Destroy(gameObject);
        } else {
            Destroy(gameObject);
        }
    }
}
