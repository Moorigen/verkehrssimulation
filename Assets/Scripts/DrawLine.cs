﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public static class DrawLine {

    private static Transform buffer;
    public static void ClearBuffer() {
        buffer = null;
    }

    public static void AddDrawPoint(Transform point, Color color) {
        if (buffer) {
            DrawLineBetween(buffer.parent, buffer, point, color);
            buffer = null;
        } else {
            buffer = point;
        }
    }

    public static GameObject DrawLineBetween(Transform parent, Transform start, Transform end, Color color, bool isEdge = true, float width = 1f) {
        
        GameObject line = new GameObject($"{start.name}|{end.name}");
        line.transform.SetParent(parent);

        Image img = line.AddComponent<Image>();
        img.color = color;
        img.type = Image.Type.Sliced;

        line.AddComponent<Line>().Init(start, end, isEdge, width);

        if (isEdge) {
            DataManager.AddLine(new NodePair(start.gameObject, end.gameObject), line.GetComponent<Line>());
            GameObject text = new GameObject(line.name + "T");
            Transform textTf = text.transform;
            textTf.SetParent(line.transform.parent);
            textTf.position = line.transform.position + Vector3.right;
            textTf.localScale = Vector3.one;
            Text textT = text.AddComponent<Text>();
            text.GetComponent<RectTransform>().sizeDelta = new Vector2(75f, 75f);
            textT.font = Font.CreateDynamicFontFromOSFont("Arial", 40);
            textT.text = "0";
            textT.fontSize = 40;
            textT.alignment = TextAnchor.MiddleCenter;
            text.AddComponent<BoxCollider2D>().size = text.GetComponent<RectTransform>().sizeDelta;
            text.AddComponent<LineText>().edge = line.transform;
        }

        return line;
    }
}
